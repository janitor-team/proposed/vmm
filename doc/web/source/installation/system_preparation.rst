==================
System Preparation
==================
.. _doveauth:

We have to create a system user, named `doveauth`.
The `doveauth` user will execute Dovecot's authentication processes.

We will also create an additional system group, named `dovemail`.
The GID of the group `dovemail` will be the supplementary GID for all
mail related Dovecot processes, e.g. the `dict` service for quota limits.

And finally we will create the ``base_directory``, with it's subdirectories.
It is the location for all domain directories and the virtual user's home
directories.

The example below shows the steps executed on a Debian GNU/Linux system.

.. code-block:: console

 root@host:~# adduser --system --home /nonexistent --no-create-home --group \
 > --disabled-login --gecos "Dovecot IMAP/POP3 authentication user" doveauth
 root@host:~# addgroup --system dovemail
 root@host:~# mkdir /srv/mail
 root@host:~# cd /srv/mail
 root@host:/srv/mail# mkdir 0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z
 root@host:/srv/mail# chmod 771 /srv/mail
 root@host:/srv/mail# chmod 751 /srv/mail/*

.. include:: ../ext_references.rst
